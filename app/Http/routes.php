<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*Route::get('basic1', function () {
    return 'Hello World';
});
Route::post('basic2', function () {
        return 'basic2';
});
Route::match(['get','post'],'multy1', function () {
    return 'multy1';
});



Route::group(['prefix'=>'member'],function (){
    Route::get('user/center',['as'=>'center',function(){
        return route('center');
    }]);
    Route::any('multy2', function () {
        return 'member-multy2';
    });
});

Route::any('member/{id}',['uses'=>'MemberController@info'])
->where('id','[0-9]+');*/

Route::group(['middleware'=>['web']],function (){

    Route::get('admin/login','admin\LoginController@login');

    Route::get('/', function () {
        return view('welcome');
    });

});


